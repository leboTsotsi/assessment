﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assessment;
using System.Linq;
using System;
using System.IO;

namespace AssessmentTest
{
    [TestClass]
    public class ComputeUserTweetsTests
    {
        [TestMethod]

        public void SaveTweepAndFollower()
        {
            var computeUserTweets = new ComputeUserTweets();
            var users = computeUserTweets.GetUsers();
            var name = "Lebohang";
            var follower = "Pearlcy";

            computeUserTweets.StoreUserData(name, follower);
            
            Assert.AreEqual(true, users.Exists(u => u.Name == follower));
            Assert.AreEqual(true, users.Exists(u => u.Name == name));
            Assert.AreEqual(true, users.Where(u => u.Name == name).First().Followers.Exists(u => u.Name == follower));            
        }

        [TestMethod]
        public void TestSaveTweet()
        {
            var computeUserTweets = new ComputeUserTweets();
            var name = "Lebohang";
            var message = "Save my test tweet";

            computeUserTweets.StoreUserData(name, null);
            computeUserTweets.StoreTweet(name, message);

            var tweets = computeUserTweets.GetTweets();
            var users = computeUserTweets.GetUsers();
            var isTweetAssignedToCorrectUser = false;
            var userTweetIds = users.Where(u => u.Name == name).FirstOrDefault()?.TweetIds;

            foreach(var tweetId in userTweetIds)
            {
                if(tweets.Where(t => t.Id == tweetId).FirstOrDefault()?.Message == message)
                    isTweetAssignedToCorrectUser = true;
            }
            
            Assert.AreEqual(true, tweets.Any(t => t.Message == message));
            Assert.AreEqual(true, isTweetAssignedToCorrectUser);
        }

        [TestMethod]
        public void TestDeriveUserLine()
        {
            var computeUserTweets = new ComputeUserTweets();
            var users = computeUserTweets.GetUsers();
            var line = "Ward follows Alan";
            var name = line.Split(new[] { "follows" }, StringSplitOptions.None)[1].Trim();
            var follower = line.Split(new[] { "follows" }, StringSplitOptions.None)[0].Trim();

            computeUserTweets.DeriveLine(ComputeUserTweets.FileLoaderEnum.Users, name, follower);

            Assert.AreEqual(true, users.Exists(u => u.Name == follower));
            Assert.AreEqual(true, users.Exists(u => u.Name == name));
            Assert.AreEqual(true, users.Where(u => u.Name == name).First().Followers.Exists(u => u.Name == follower));
        }

        [TestMethod]
        public void TestDeriveMultipleUserLine()
        {
            var computeUserTweets = new ComputeUserTweets();
            var users = computeUserTweets.GetUsers();
            var line = "Ward follows Alan, Martin";
            var name = line.Split(new[] { "follows" }, StringSplitOptions.None)[1].Trim();
            var follower = line.Split(new[] { "follows" }, StringSplitOptions.None)[0].Trim();
            
            computeUserTweets.DeriveLine(ComputeUserTweets.FileLoaderEnum.Users, name, follower);

            foreach(var item in name.Split(','))
            {
                Assert.AreEqual(true, users.Exists(u => u.Name == follower));
                Assert.AreEqual(true, users.Exists(u => u.Name == item.Trim()));
                Assert.AreEqual(true, users.Where(u => u.Name == item.Trim()).First().Followers.Exists(u => u.Name == follower));
            }
        }

        [TestMethod]
        public void TestReadIfFileExists()
        {
            var userFileExists = File.Exists(Global.USER_FILE_PATH);
            var tweetsFileExists = File.Exists(Global.TWEET_FILE_PATH);
            
            Assert.AreEqual(true, userFileExists);
            Assert.AreEqual(true, tweetsFileExists);
        }

        [TestMethod]
        public void TestLineRequiredInfo()
        {
            var userLines = File.ReadAllLines(Global.USER_FILE_PATH).ToList();
            var tweetLines = File.ReadAllLines(Global.TWEET_FILE_PATH).ToList();

            var doAllUserLinesContainFollows = userLines.All(l => l.Contains("follows"));
            var doAllTweeLinesContainSeparator = tweetLines.All(l => l.Contains(">"));

            Assert.AreEqual(true, doAllUserLinesContainFollows);
            Assert.AreEqual(true, doAllTweeLinesContainSeparator);
        }
    }
}
