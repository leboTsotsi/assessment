﻿using Assessment.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Assessment
{
    public class ComputeUserTweets
    {
        public enum FileLoaderEnum { Users, Tweets }
        private List<User> _users;
        private List<Tweet> _tweets;

        public ComputeUserTweets()
        {
            _users = new List<User>();
            _tweets = new List<Tweet>();
        }

        public void LoadFilesFromFiles(string userPath, string tweetPath)
        {
            ReadLinesFromFile(FileLoaderEnum.Users, File.ReadAllLines(userPath));
            ReadLinesFromFile(FileLoaderEnum.Tweets, File.ReadAllLines(tweetPath));
        }

        public void ReadLinesFromFile(FileLoaderEnum @enum,string[] lines)
        {
            switch (@enum)
            {
                case FileLoaderEnum.Users:
                    {
                        foreach (var line in lines)
                        {
                            if (!line.Contains("follows"))

                                throw new ArgumentException($"Please make sure that the user.txt file has the required info.");
                            
                            var splitLineArr = line.Split(new[] { "follows" }, StringSplitOptions.None);
                            DeriveLine(@enum, splitLineArr[1].Trim(), splitLineArr[0].Trim());
                        }
                    }
                    break;
                case FileLoaderEnum.Tweets:
                    {
                        foreach (var line in lines)
                        {
                            if (!line.Contains(">"))
                                throw new ArgumentException($"Please make sure that the tweet.txt file has the required info.");
            
                            var splitLineArr = line.Split('>');
                            DeriveLine(@enum, splitLineArr[0].Trim(), splitLineArr[1].Trim());
                        }
                    }
                    break;
            }
        }

        public void DeriveLine(FileLoaderEnum @enum, string firstValue, string secondValue)
        {
            if (string.IsNullOrEmpty(firstValue ) || string.IsNullOrEmpty(secondValue ))
                throw new Exception("Please ensure that a user and/or follower is provided. Check in user.txt file");

            switch (@enum)
            {
                case FileLoaderEnum.Users:
                    {
                        if (firstValue.Contains(","))
                            foreach (var item in firstValue.Split(',')) //Check for multiple users
                                StoreUserData(item, secondValue);
                        else
                            StoreUserData(firstValue, secondValue);

                    }
                    break;
                case FileLoaderEnum.Tweets:
                    {
                        StoreTweet(firstValue, secondValue);
                    }
                    break;
            }
        }

        public void StoreTweet(string name, string message)
        {
            if (_users.Exists(u => u.Name == name))
            {
                var user = _users.Where(u => u.Name == name).First();
                if (name.Length > 140)
                    throw new Exception("Tweet must be less or equal to 140 characters}");

                var tweet = new Tweet
                {
                    Id = Guid.NewGuid().ToString(),
                    Message = message,
                    UserId = user.Id,
                    Timestamp = DateTime.Now
                };

                _tweets.Add(tweet);
                user?.TweetIds?.Add(tweet.Id); //assign tweets to user

                foreach (var follower in user?.Followers) //assign tweets to user followers
                    _users.Where(u => u.Id == follower.FollowingID).FirstOrDefault()?.TweetIds.Add(tweet.Id);
            }
            else
                throw new ArgumentNullException($"User {name} does not exist. Tweet could not be added");
        }

        public void StoreUserData(string name, string follower)
        {
            //Save follower account/record first
            if (!string.IsNullOrEmpty(follower) && !_users.Exists(u => u.Name == follower.Trim()))
                _users.Add(new User { Id = Guid.NewGuid().ToString(), Name = follower.Trim(), Followers = new List<Follower>(), TweetIds = new List<string>() });

            var followers = new List<Follower>();
            if (_users.Exists(u => u.Name == name.Trim() ))
            {
                foreach (var user in _users)
                {
                    if (user.Name == name )
                    {
                        followers = user.Followers; //retrive current user followers
                        followers = followers.Where(f => f.Name != follower ).ToList();
                        followers.Add(new Follower { Name = follower , FollowingID = _users.Where(u => u.Name == follower.Trim() ).FirstOrDefault()?.Id });
                        user.Followers = followers;
                    }
                }
            }
            else
            {
                followers.Add(new Follower { Name = follower , FollowingID = _users.Where(u => u.Name == follower ).FirstOrDefault()?.Id });
                _users.Add(new User { Id = Guid.NewGuid().ToString(), Name = name.Trim() , Followers = followers, TweetIds = new List<string>() }); //Add user if record doesn't exist
            }
        }

        public List<User> GetUsers()
        {
            return _users;
        }

        public List<Tweet> GetTweets()
        {
            return _tweets;
        }
    }
}
