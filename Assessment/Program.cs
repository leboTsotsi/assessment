﻿using Assessment.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Assessment
{
    class Program
    {
        private static List<User> _users;
        private static List<Tweet> _tweets;

        static void Main(string[] args)
        {
            try
            {
                var objComputeUserTweets = new ComputeUserTweets();
                objComputeUserTweets.LoadFilesFromFiles(Global.USER_FILE_PATH, Global.TWEET_FILE_PATH);

                _users = objComputeUserTweets.GetUsers().OrderBy(key => key.Name).OrderBy(key => key.Name).ToList();//Order users alphabetically
                _tweets = objComputeUserTweets.GetTweets();

                foreach (var user in _users)
                    ConstructOutput(user);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"{ex.Message}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}\nPlease verify that the file exists and in provided location.");
            }

            Console.WriteLine("\n\n---------------------\nPress any key to exit");
            Console.ReadKey();
        }

        private static void ConstructOutput(User user)
        {
            Console.WriteLine(user.Name);
            foreach (var tweetId in user.TweetIds)
            {
                if (_tweets.Exists(t => t.Id == tweetId))
                {
                    var tweet = _tweets.Where(t => t.Id == tweetId).FirstOrDefault();
                    var tweep = _users.Where(u => u.Id == tweet.UserId).FirstOrDefault()?.Name;
                    Console.WriteLine($"\t@{tweep}: {tweet.Message}");
                }
            }
        }
    }
}
