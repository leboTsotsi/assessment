﻿using System;

namespace Assessment.Model
{
    public class Tweet
    {
        public string Id { get; set; }
        public string Message { get; set; }
        public string UserId { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
