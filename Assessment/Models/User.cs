﻿using System.Collections.Generic;

namespace Assessment.Model
{
    public class User
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<Follower> Followers { get; set; }
        public List<string> TweetIds { get; set; }
    }

    public class Follower
    {
        public string Name { get; set; }
        public string FollowingID { get; set; }
    }
}
